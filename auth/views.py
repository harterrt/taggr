from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

def register(request):
    if request.method == 'GET':
        return render(request, 'registration/register.html')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        uu = User.objects.create_user(username, password=password)
        return redirect('home')

