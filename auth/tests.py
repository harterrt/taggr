"""
"""

from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User

url_prefix = '/auth'

class TestAuth(TestCase):
    def setUp(self):
        self.client = Client()

        self.new_user = {'username':'new_guy', 'password':'password'}
        self.exist_user = {'username':'old_head', 'password':'password'}

        user = User.objects.create_user(username=self.exist_user['username'],
            password=self.exist_user['password'])
        self.exist_user['pk'] = user.pk


    def test_register(self):
        """
        Ensure user can see registration page.
        """
        url = url_prefix + '/register/'
        username = self.new_user['username']
        password = self.new_user['password']

        #Can we even see the registration page?
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        
        #Make sure username is not already in the DB
        self.assertRaises(User.DoesNotExist, User.objects.get, username=username)

        #Make sure we can add users
        self.client.post(url, {'username':username, 'password':password})
        self.assertEqual(response.status_code, 200)
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            self.fail("Cannot find username in database.")
        
    def test_login(self):
        """
        Ensure a user can login.
        """
        login_url = url_prefix + '/login/'
        logout_url = url_prefix + '/logout/'
        username = self.exist_user['username']
        password = self.exist_user['password']

        #Make sure we can access the login page
        response = self.client.get(login_url)
        self.assertEqual(response.status_code, 200)

        #Add user, test 200 response, confirm proper user loggedin.
        self.client.post(login_url, {'username':username, 'password':password})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.client.session['_auth_user_id'],
            self.exist_user['pk'])

	#Log the user out
	response = self.client.get(logout_url)
	self.assertEqual(response.status_code, 200)
	self.assertEqual(self.client.session.has_key('_auth_user_id'), False)

